
var path = require('path');
var exec = require('child_process').exec;

var config = {
    reports: './reports',
    features: './features/**/*.feature',
    browser: 'chrome',
    timeout: 15000
};

process.argv.splice(2);

console.log(process.cwd())



// rewrite command line switches for cucumber

// add cucumber world as first required script (this sets up the globals)
/* process.argv.push('-r');
process.argv.push(path.resolve(__dirname, 'runtime/world.js')); */

process.argv.push(path.resolve(__dirname, 'api/features/'));

exec('cucumber '+config.features, function (error, stdout, stderr) {
    if (error) {
        throw error;
    } else {
        console.log(stdout);
        console.log(stderr);
    }
});

exports.test = function (){

};